#!/usr/bin/env bash

ROOT_UID=0
DEST_DIR=
THEME_NAME=Papirus-Mace

# Destination directory
if [ "$UID" -eq "$ROOT_UID" ]; then
  DEST_DIR="/usr/share/icons"
else
  DEST_DIR="$HOME/.local/share/icons"
fi

copy_variant(){
mkdir -p $DEST_DIR
cp -r Papirus-Mace* $DEST_DIR/
}

cd "$(dirname "$0")"
copy_variant
echo
echo "Themes installed to $DEST_DIR"
echo

exit