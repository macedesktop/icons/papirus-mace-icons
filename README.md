# Papirus-Mace

<img src="https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-icon-theme/master/preview.png">

## Description

A collection of slightly modified [Papirus](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) Icons for use with Mace.

## Installation

Put them in the relevant places.

Or use the install script.

Installs into $HOME by default or into /usr/share if root.

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)
